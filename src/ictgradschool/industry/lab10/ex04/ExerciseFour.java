package ictgradschool.industry.lab10.ex04;

/**
 * Created by anhyd on 27/03/2017.
 */
public class ExerciseFour {

    public static void main(String[] args) {
        ExerciseFour test = new ExerciseFour();
        int[] array = {3, 4, 3, 6, 7, 1};
        String text = "kk";
        System.out.println(test.isPalindrome(text));
    }

    /**
     * Returns the sum of all positive integers between 1 and num (inclusive).
     */
    public int getSum(int num) {

        // TODO Implement a recursive solution to this method.
        if(num == 1){
            return 1;
        }
        return num + getSum(num -1);

    }


    /**
     * Returns the smallest value in the given array, between the given first (inclusive) and second (exclusive) indices
     *
     * @param nums the array
     * @param firstIndex the inclusive lower index
     * @param secondIndex the exclusive upper index
     */
    public int getSmallest(int[] nums, int firstIndex, int secondIndex) {

        // TODO Implement a recursive solution to this method.

        if(secondIndex > nums.length - 1){
            secondIndex = nums.length -1;
        }
        if(firstIndex == secondIndex ){
            return nums[secondIndex];
        }

        firstIndex++;

        return Math.min(nums[firstIndex -1], getSmallest(nums, firstIndex, secondIndex));
    }

    /**
     * Prints all ints from n down to 1.
     */
    public void printNums1(int n) {

        // TODO Implement a recursive solution to this method.
        if(n == 1) {
            System.out.println(n);
            return;
        }
        else {
            System.out.println(n);
            printNums1(n - 1);
        }
    }

    /**
     * Prints all ints from 1 up to n.
     */
    public void printNums2(int n) {

        // TODO Implement a recursive solution to this method.
        if(n == 1) {
            System.out.println(n);
            return;
        }
        else {
            printNums2(n - 1);
            System.out.println(n);
        }

    }

    /**
     * Returns the number of 'e' and 'E' characters in the given String.
     *
     * @param input the string to check
     */
    public int countEs(String input) {
        int num = 0;
        // TODO Implement a recursive solution to this method.
        if(input.length() == 1){
            if(input.equals("e") || input.equals("E")){
                num = 1;
            }
            return num;
        }

        if(input.charAt(input.length() - 1) == 'e' || input.charAt(input.length() - 1) == 'E'){
            num++;
        }
        return countEs(input.substring(0, input.length() - 1)) + num;

    }

    /**
     * Returns the nth number in the fibonacci sequence.
     */
    public int fibonacci(int n) {

        // TODO Implement a recursive solution to this method.
        //I excluded 0 from the fibonacci numbers. To include it, change to n <= 1
        if(n < 1){
            return n;
        }
        else if (n == 1){
            return n;
        }

        return fibonacci(n -1) + fibonacci(n-2);
    }

    /**
     * Returns true if the given input String is a palindrome, false otherwise.
     *
     * @param input the String to check
     */
    public boolean isPalindrome(String input) {

        // TODO Implement a recursive solution to this method.
        int length = input.length();


        if(length <= 2){
            if(length <= 1){
                return true;
            }
            else if(length == 2){
                if(input.charAt(0) == input.charAt(1)) {
                    return true;
                }
            }
            return false;
        }

        boolean current;
        if(input.charAt(0) == input.charAt(length - 1)){
            current = true;
        }
        else{
            current = false;
        }

        return isPalindrome(input.substring(1, input.length() - 1)) && current;
    }

}
