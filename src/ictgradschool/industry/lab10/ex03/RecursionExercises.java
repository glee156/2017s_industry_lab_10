package ictgradschool.industry.lab10.ex03;

/**
 * Created by glee156 on 5/12/2017.
 */
public class RecursionExercises {
    private double bar ( double x , int n ) {
        if ( n > 1)
            return x * bar ( x , n - 1 );
        else if ( n < 0)
            return 1.0 / bar ( x , - n );
        else
            return x;
    }

    public static void main(String[] args) {
        RecursionExercises test = new RecursionExercises();
        double result = test.bar(3, -2);
        System.out.println(result);
    }
}

